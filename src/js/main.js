'use strict';

$(document).ready(function () {
    // play initial animation for one second if some of the content
    // cant be loaded for some reason
    setTimeout(function () {
        $('body').addClass('loaded');
    }, 3000);
});
// stop the animation in case everithing is loaded
// $(window).on('load', function () {
//     $('body').addClass('loaded');
// });
// test
