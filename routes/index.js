var express = require('express');
var router = express.Router();

var webTitle = 'hgill.io|mygnu.me';
/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {
        title: webTitle + 'Home'
    });
});
/* GET test page. */
router.get('/myworks', function (req, res, next) {
    res.render('myworks', {
        title: webTitle + 'myworks'
    });
});

module.exports = router;
