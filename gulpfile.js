const gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    mincss = require('gulp-cssnano'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    // uncss = require('gulp-uncss'),
    jade = require('gulp-jade'),
    pngquant = require('imagemin-pngquant');

gulp.task('jade', function () {
    gulp.src('./views/index.jade')
        .pipe(jade())
        .pipe(gulp.dest('./public'));
});

gulp.task('css', () => {
    gulp.src([
            // './views/css/loader.css',
            // './views/css/normalize.css',
            './src/css/*.css'
        ])
        .pipe(concat('app.css'))
        // .pipe(uncss({
        //     html: ['http://localhost:3000']
        // }))
        .pipe(mincss())
        .pipe(gulp.dest('./public/css'));
});

gulp.task('scripts', () => {
    gulp.src([
            // './src/js/vendor/jquery-1.11.3.min.js',
            './src/js/*.js'
        ])
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/js'));
});

gulp.task('images', () => {
    gulp.src("./src/images/*")
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: true
            }],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('public/images'));
});
gulp.task('watch', () => {
    gulp.watch(['./src/css/*.css'], [
        'css'
    ]);
    gulp.watch(['./src/js/*.js'], ['scripts']);
    // gulp.watch(['./views/*.jade'], ['jade']);
});

gulp.task('default', ['css', 'scripts', 'images']);
